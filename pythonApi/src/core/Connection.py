from pymongo import MongoClient


def connect(config):
    client = MongoClient(config.mongodbConnection('host'), config.mongodbConnection('port'))
    db = client.local
    col = db.mis_usuarios

    return col
