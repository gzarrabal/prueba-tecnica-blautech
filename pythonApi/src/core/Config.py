import yaml


class Config:

    def __init__(self):
        with open('config.yml', 'r') as f:
            self.doc = yaml.safe_load(f)

    def flaskPort(self):
        return self.doc["flask"]["port"]

    def mongodbConnection(self, key):
        return self.doc["mongodb"][key]
