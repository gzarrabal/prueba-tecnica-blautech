from src.resources import schemaResponse
from src.core.Connection import connect


class FindOneServices:

    def __init__(self, config):
        self.config = config

    def service(self):
        cnn = connect(self.config)

        result = cnn.find()

        response = []
        for res in result:
            response.append({
                'name': res["name"],
                'age': res["age"],
                'mail': res["mail"],
                'password': res["password"]
            })

        return schemaResponse.correctResponse("la busqueda se realizó correctamente", response)
