from src.resources import schemaResponse
from src.core.Connection import connect


class CreateService:

    def __init__(self, request, config):
        self.request = request
        self.config = config

    def service(self):
        cnn = connect(self.config)
        cnn.insert_one(
            {
                'name': self.request["name"],
                'age': self.request["age"],
                'mail': self.request["mail"],
                'password': self.request["password"]
            }
        )
        return schemaResponse.correctResponse("se insertó el registro correctamente", {"status": True})
