from src.resources import schemaResponse
from src.core.Connection import connect


class UpdateService:

    def __init__(self, request, config):
        self.name = request["name"]
        self.age = request["age"]
        self.mail = request["mail"]
        self.password = request["password"]
        self.config = config

    def service(self):
        cnn = connect(self.config)
        cnn.update_one(
            {
                "mail": self.mail
            },
            {
                "$set": {
                    'name': self.name,
                    'age': self.age,
                    'mail': self.mail,
                    'password': self.password
                }
            }
        )
        return schemaResponse.correctResponse("se modificó el registro correctamente", {"status": True})
