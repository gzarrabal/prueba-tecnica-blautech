from src.resources import schemaResponse
from src.core.Connection import connect


class DeleteService:

    def __init__(self, mail, config):
        self.mail = mail
        self.config = config

    def service(self):
        cnn = connect(self.config)
        cnn.delete_one({"mail": self.mail})

        return schemaResponse.correctResponse("se eliminó el registro correctamente", {"status": True})
