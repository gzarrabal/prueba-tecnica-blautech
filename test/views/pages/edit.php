<div class="modal fade" id="editModal" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="container">
					<form>
						<h3 class="text-center py-4">{Editar Registro}</h3>
						<div class="form-group">
							<label class="text-muted small">Nombre</label>
							<input type="text" class="form-control rounded-0">
						</div>
						<div class="form-group">
							<label class="text-muted small">Edad</label>
							<input type="number" class="form-control rounded-0">
						</div>
						<div class="form-group">
							<label class="text-muted small">Correo Electrónico</label>
							<input type="email" class="form-control rounded-0">
						</div>
						<div class="form-group">
							<label class="text-muted small">Contraseña</label>
							<input type="password" class="form-control rounded-0">
						</div>
						<button type="submit" class="btn btn-info btn-block mt-5">Editar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>