

<div class="container-fluid h-100">
	<div class="row h-100">
		<div class="col-md-3 shadow bg-register">
			<h3 class="text-center py-5">{Agregar Nuevo}</h3>
			<form method="post" role="form">
				<div class="form-group">
					<label class="text-muted small">Nombre</label>
					<input name="inp-name" type="text" class="form-control rounded-0">
				</div>
				<div class="form-group">
					<label class="text-muted small">Edad</label>
					<input name="inp-age" type="number" class="form-control rounded-0">
				</div>
				<div class="form-group">
					<label class="text-muted small">Correo Electrónico</label>
					<input name="inp-mail" type="email" class="form-control rounded-0">
				</div>
				<div class="form-group">
					<label class="text-muted small">Contraseña</label>
					<input name="inp-password" type="password" class="form-control rounded-0">
				</div>
				<button type="submit" class="btn btn-info btn-block mt-5">Registrar</button>

				<?php 

				if(isset($_POST["inp-mail"])){
					$controllerCreate = new UserController();

					$datos = array('name' => $_POST["inp-name"],
						'age' => $_POST["inp-age"],
						'mail' => $_POST["inp-mail"],
						'password' => $_POST["inp-password"]);

					$controllerCreate->create($datos);
				}

				?>

			</form>
		</div>

		<!-- find -->
		
		<div class="col-md-9">
			<div class="row">

				<?php 
				$controllerFind = new UserController();
				$find = $controllerFind->find();
		 		?>
		 		<?php foreach ($find["response"] as $key => $value): ?>

		 			<div class="col-12 col-md-3 p-3">
					<div class="card border-0 rounded-lg shadow-sm text-center">
						<div class="age rounded-circle mt-4"><?php echo $value["age"] ?></div>
						<div class="card-body">
							<h5 class="card-title mb-0"><?php echo $value["name"] ?></h5>
							<p class="text-muted"><?php echo $value["mail"] ?></p>
							<div class="text-right">
								<button type="button" class="btn btn-warning btn-sm rounded-circle" data-toggle="modal" data-target="#editModal"><i class="far fa-edit"></i></button>
								<form method="post" role="form">
									<input name="inp-id-hidden-<?php echo $value["id"] ?>" type="hidden" class="form-control rounded-0" value="<?php echo $value["mail"] ?>">
								<button type="submit" class="btn btn-danger btn-sm rounded-circle mt-2"><i class="fas fa-ban"></i></button>

								<?php 

								if(isset($_POST["inp-id-hidden-".$value["id"]])){
									$controllerDelete = new UserController();

									$datos = array('id' => $value["id"],
										'mail' => $value["mail"]);

									$controllerDelete->delete($datos);
								}

								?>
								</form>
							</div>
						</div>
					</div>
				</div>

		 		<?php endforeach ?>
			</div>
		</div>
	</div>    
</div>


<?php include "edit.php"; ?>