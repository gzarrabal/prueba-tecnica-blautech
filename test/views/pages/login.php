    <div class="container h-100">
        <div class="row h-100">
            <div class="col-md-5 my-auto mx-auto bg-login shadow">
                <h3 class="text-center pb-5">{Bienvenido}</h3>
                <form>
                  <div class="form-group">
                    <label class="text-muted small">Correo Electrónico</label>
                    <input type="email" class="form-control rounded-0">
                  </div>
                  <div class="form-group">
                    <label class="text-muted small">Contraseña</label>
                    <input type="password" class="form-control rounded-0">
                  </div>
                  <button type="submit" class="btn btn-info btn-block mt-5">Ingresar</button>
                </form>
            </div>
        </div>    
    </div>