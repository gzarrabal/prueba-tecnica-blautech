<?php

class UserController 
{
	private $client;

    public function __construct()
    {
        $this->client = new Clients('http://127.0.0.1:5002/gateway/');
    }

	public function create($request){
		$response = $this->client->newPost('create/v1', $request);
		return $response;
	}

	public function find(){
		$response = $this->client->get('find/v1');
		return $response;
	}

	public function update(){
		$response = $this->client->json('update/v1');
		return $response;
	}

	public function delete($request){
		$response = $this->client->newPost('delete/v1', $request);
		return $response;
	}
}