<?php
use GuzzleHttp\Client;
class Clients
{

    private $context;

    public function __construct($context)
    {
        $this->context = $context;
    }

    public function get($path, $key=null, $value=null){

        if ($key == null || $value == null) {
            $contextPath = $this->context.$path;
        }else{
            $contextPath = $this->context.$path.'/'.$key.'/'.$value;
        }

        $response = json_decode(file_get_contents($contextPath), true);

        return $response;
    }

    public function post($path, $request, $contentType){

        $requestPost = http_build_query($request);

        $configHttp = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => array('Content-Type: '.$contentType),
                'content' => $requestPost
            )
        );

        $contexto = stream_context_create($configHttp);

        $response = json_decode(file_get_contents($this->context.$path, false, $contexto), true);

        return $response;
    }

    public function json($path, $request, $contentType){


        $configHttp = array('http' =>
            array(
                'method' => 'POST',
                'header' => array('Content-Type: '.$contentType),
                'content' => json_encode($request)
            )
        );

        $contexto = stream_context_create($configHttp);

        $response = json_decode(file_get_contents($this->context.$path, null, $contexto), true);

        return $response;
    }


    public function newPost($path, $request){
    	// Consumir api
			$client = new Client();


			$res = $client->post($this->context.$path,
				[
					'headers' => [
						'Content-Type' => 'application/x-www-form-urlencoded'
					],
					'form_params' => $request
				]);

			if ($res->getStatusCode() == '200'){

				$response = json_decode($res->getBody(), true);
			}
		}
	}
