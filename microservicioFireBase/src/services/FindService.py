from src.resources import schemaResponse
from src.core.Connection import Connection


class FindService:

    def __init__(self, config):
        self.config = config

    def service(self):
        cnn = Connection(self.config)
        result = cnn.find()

        response = []

        if cnn.find() is not None:
            for res in cnn.find():
                response.append({
                    'id': res,
                    'name': result[res]["name"],
                    'age': result[res]["age"],
                    'mail': result[res]["mail"],
                    'password': result[res]["password"],
                })

        return schemaResponse.correctResponse("la busqueda se realizó correctamente", response)
