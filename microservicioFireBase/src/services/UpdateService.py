from src.resources import schemaResponse
from src.core.Connection import Connection


class UpdateService:

    def __init__(self, request, config):
        self.request = request
        self.config = config

    def service(self):
        cnn = Connection(self.config)
        result = cnn.update(self.request)

        return schemaResponse.correctResponse("se modificó el registro correctamente", {"status": True})
