from src.resources import schemaResponse
from src.core.Connection import Connection


class DeleteService:

    def __init__(self, id, config):
        self.id = id
        self.config = config

    def service(self):
        cnn = Connection(self.config)
        cnn.delete(self.id)

        return schemaResponse.correctResponse("se eliminó el registro correctamente", {"status": True})
