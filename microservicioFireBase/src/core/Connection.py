from firebase import firebase
import pyrebase


class Connection:

    def __init__(self, config):
        self.firebase_conection = firebase.FirebaseApplication(config.firebaseConnection("host"), None)
        self.collection = config.firebaseConnection("collection")

    def create(self, datos):
        self.firebase_conection.post(self.collection, datos)

    def find(self):
        return self.firebase_conection.get(self.collection, '')

    def update(self, request):

        keys = ["name", "age", "mail", "password"]

        for key in keys:
            if key in request:
                self.firebase_conection.put(self.collection + "/" + request["id"], key, request[key])

    def delete(self, id):
        self.firebase_conection.delete(self.collection, id)

    def configAuth(self):
        firebaseAuthConfig = {
            'apiKey': "AIzaSyD8TZrIibb_1Ea8Um2m6QYYMLx3RgchaJM",
            'authDomain': "autenticacion-python-11ed6.firebaseapp.com",
            'databaseURL': "https://autenticacion-python-11ed6.firebaseio.com",
            'projectId': "autenticacion-python-11ed6",
            'storageBucket': "autenticacion-python-11ed6.appspot.com",
            'messagingSenderId': "869946855254",
            'appId': "1:869946855254:web:0c128ec1f7a128e82090f7",
            'measurementId': "G-8XBCHDVX83"
        }
