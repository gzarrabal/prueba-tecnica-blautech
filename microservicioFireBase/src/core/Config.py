import yaml


class Config:

    def __init__(self):
        with open('config.yml', 'r') as f:
            self.doc = yaml.safe_load(f)

    def flaskPort(self):
        return self.doc["flask"]["port"]

    def firebaseConnection(self, key):
        return str(self.doc["firebase"][key])
