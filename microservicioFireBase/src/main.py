from src.core.Config import Config
from src.services.CreateService import CreateService
from src.services.FindService import FindService
from src.services.UpdateService import UpdateService
from src.services.DeleteService import DeleteService
from flask import Flask, jsonify, request

app = Flask(__name__)
config = Config()


@app.route('/firebase/create/v1', methods=['PUT'])
def create():
    my_request = {
        'name': request.form['name'],
        'age': request.form['age'],
        'mail': request.form['mail'],
        'password': request.form['password'],
    }

    create = CreateService(my_request, config)
    return jsonify(create.service())


@app.route('/firebase/find/v1', methods=['GET'])
def find():
    finds = FindService(config)
    return jsonify(finds.service())


@app.route('/firebase/update/v1', methods=['PATCH'])
def update():

    my_request = request.json
    update = UpdateService(my_request, config)
    return jsonify(update.service())


@app.route('/firebase/delete/v1', methods=['DELETE'])
def delete():

    id = request.form['id']

    delete = DeleteService(id, config)
    return jsonify(delete.service())


if __name__ == '__main__':
    app.run(port=config.flaskPort())
