from src.resources import schemaResponse
from src.rest.Client import Client


class UpdateService:

    def __init__(self, request, config):
        self.request = request
        self.config = config

    def service(self):
        client_mongo = Client(self.config.mongodbContext())
        response_mongo = client_mongo.patch('update/v1', self.request)

        if response_mongo.status_code is 200:
            client_firebase = Client(self.config.firebaseContext())
            response_firebase = client_firebase.patch('update/v1', self.request)
            response = schemaResponse.correctResponse("se actualizó el registro correctamente", {"status": True})
        else:
            response = schemaResponse.failedResponse(2, 2.1, "el servicio contesto de manera inesperada")

        return response
