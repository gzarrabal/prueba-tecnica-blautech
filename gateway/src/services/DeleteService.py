from src.resources import schemaResponse
from src.rest.Client import Client


class DeleteService:

    def __init__(self, id, mail, config):
        self.id = id
        self.mail = mail
        self.config = config

    def service(self):
        client_mongo = Client(self.config.mongodbContext())
        response_mongo = client_mongo.delete('delete/v1', {'mail': self.mail})

        if response_mongo.status_code is 200:
            client_firebase = Client(self.config.firebaseContext())
            response_firebase = client_firebase.delete('delete/v1', {'id': self.id})
            response = schemaResponse.correctResponse("el registro se eliminó correctamente", {"status": True})
        else:
            response = schemaResponse.failedResponse(2, 2.1, "el servicio contesto de manera inesperada")

        return response

        return schemaResponse.correctResponse("se eliminó el registro correctamente", {"status": True})
