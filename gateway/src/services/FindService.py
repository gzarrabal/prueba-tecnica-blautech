from src.resources import schemaResponse
from src.rest.Client import Client


class FindService:

    def __init__(self, config):
        self.config = config

    def service(self):
        client = Client(self.config.firebaseContext())
        result = client.get('find/v1').json()["response"]

        return schemaResponse.correctResponse("la busqueda se realizó correctamente", result)
