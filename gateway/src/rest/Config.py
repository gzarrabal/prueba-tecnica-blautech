import yaml


class Config:

    def __init__(self):
        with open('config.yml', 'r') as f:
            self.doc = yaml.safe_load(f)

    def flaskPort(self):
        return self.doc["flask"]["port"]

    def firebaseContext(self):
        return str(self.doc["microservice_firebase"]["context"])

    def mongodbContext(self):
        return str(self.doc["microservice_mongodb"]["context"])
