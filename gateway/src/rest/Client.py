import requests


class Client:

    def __init__(self, context):
        self.context = context

    def get(self, path):
        return requests.get(self.context + path)

    def put(self, path, request):
        return requests.put(self.context + path, data=request)

    def patch(self, path, request):
        response = requests.patch(self.context + path, json=request)
        return response

    def delete(self, path, request):
        return requests.delete(self.context + path, data=request)
