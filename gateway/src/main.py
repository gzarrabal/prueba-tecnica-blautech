from src.rest.Config import Config
from src.services.CreateService import CreateService
from src.services.FindService import FindService
from src.services.UpdateService import UpdateService
from src.services.DeleteService import DeleteService
from flask import Flask, jsonify, request

app = Flask(__name__)
config = Config()


@app.route('/gateway/create/v1', methods=['POST'])
def create():
    my_request = {
        'name': request.form['name'],
        'age': request.form['age'],
        'mail': request.form['mail'],
        'password': request.form['password'],
    }

    create = CreateService(my_request, config)
    return jsonify(create.service())


@app.route('/gateway/find/v1', methods=['GET'])
def find():
    finds = FindService(config)
    return jsonify(finds.service())


@app.route('/gateway/update/v1', methods=['POST'])
def update():

    my_request = request.json
    update = UpdateService(my_request, config)
    return jsonify(update.service())


@app.route('/gateway/delete/v1', methods=['POST'])
def delete():

    id = request.form['id']
    mail = request.form['mail']

    delete = DeleteService(id, mail, config)
    return jsonify(delete.service())


if __name__ == '__main__':
    app.run(port=config.flaskPort())
