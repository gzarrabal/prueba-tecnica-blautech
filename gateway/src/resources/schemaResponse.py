def correctResponse(message, content):
    return {
        'status': 0,
        'error': False,
        'message': message,
        'response': content
    }


def failedResponse(status, code, message):
    return {
        'status': status,
        'error': True,
        'code': code,
        'message': message
    }
